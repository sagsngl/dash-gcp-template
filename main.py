# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
from dash import dcc
from dash import html
import plotly.express as px
import numpy as np
import pandas as pd
#import seaborn as sns
#import matplotlib.pyplot as plt
import plotly.graph_objects as go

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")
weather = pd.read_csv("weather.csv.gz")

cancelled = flights[flights.dep_delay.isna()]
temp = (np.round(cancelled.loc[:,'hour'] + 1/60*cancelled.loc[:,'minute'])).values
cancelled = cancelled.assign(hour = temp)
#pd.merge(cancelled, weather,  how='left', left_on=['year','month','day'], right_on = ['year','month','day'])
w_EWR = weather.iloc[:,[0,1,2,3,4,7,10,13]]
c_EWR = cancelled[cancelled['origin'] =='EWR']
d_EWR = pd.merge(c_EWR, w_EWR,  how='left', left_on=['year','month','day','hour'], right_on = ['year','month','day','hour'])
d_EWR['name'] = d_EWR.index

fig = px.line(d_EWR, x=d_EWR.index, y="wind_speed_EWR", title='Wind Speed during cancelled flights from Newark',
             labels={
                     "wind_speed_EWR": "Wind Speed (mph)",
                     "index": "Flights"
                 },)
fig.add_shape(type='line',x0=0,y0=weather['wind_speed_EWR'].mean(),x1=len(d_EWR),y1=weather['wind_speed_EWR'].mean(),line=dict(color='Red',),xref='x',yref='y', name="Mean Speed in 2013")
fig.add_trace(go.Scatter(
    x=[3450],
    y=[10],
    text=["Mean Speed in 2013"],
    mode="text",
))
#fig.show(showlegend=True)


airport_state = airports.loc[:,['faa','state','name']]
airlines = pd.read_csv("airlines.csv.gz")
airlines = airlines.rename(columns={"name": "title"})
flight_count = flights.loc[:,['origin','dest','distance','carrier','air_time']]
df = pd.merge(flight_count, airport_state, how='left', left_on=['dest'], right_on = ['faa'])
df = pd.merge(df, airlines, on = "carrier")
fig2 = px.scatter(df.dropna(), x="name", y="title",
                 size="distance", color="origin", hover_name="title",height = 1000,
                labels={
                     "name": "Airport",
                     "title": "Airline",
                     "origin": "Origin"
                 },
                title="Distances operated by Airlines")
#fig2.show()

app.layout = html.Div(children=[
    html.H1(children='Sagar\'s Dash'),

    html.Div(children='''
        Dash: Map of wind speeds during cancelled flights from Newark(EWR) Airport  .
    '''),

    dcc.Graph(
        id='1',
        figure=fig
    ),

    html.Div(children='''
        Dash: Map of distances flown by different airlines and to where. Bubble size shows distance
    '''),

    dcc.Graph(
        id='2',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
